import {TextField, Theme, Tooltip, TooltipProps, SxProps} from "@mui/material";

export const SearchInput = (
    {
        tooltipTitle = '',
        tooltipPlacement = 'top',
        onChange,
        value,
        sx,
        ...props
    }) => {
    return (
        <Tooltip title={tooltipTitle} placement={tooltipPlacement}>
            <TextField
                id="outlined-basic"
                size='small'
                label="Поиск"
                value={value}
                onChange={onChange}
                sx={{minWidth: '300px', ...sx}}
                {...props}/>
        </Tooltip>
    )
}

