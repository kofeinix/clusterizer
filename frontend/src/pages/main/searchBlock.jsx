import {SearchInput} from "../../components/SearchInput";
import {useDeferredValue, useEffect, useState} from "react";

export const SearchBlock = ({subTree, onFilterData}) => {
    const [searchValue, setSearchValue] = useState('')
    const defferedSearchValue = useDeferredValue(searchValue)

    useEffect(() => {
        if (onFilterData) {
            onFilterData(defferedSearchValue)
        }
    }, [defferedSearchValue])

    return (
        <SearchInput
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            tooltipPlacement='top'
            tooltipTitle={`Поиск осуществляется по названию ${subTree ? 'ответа' : 'вопроса'}`}
        />
    )
}